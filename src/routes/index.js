//import { Router } from "express";
//definimos la variable ruta, que se encargara de las rutas, como un enrutador o con su nombre enrutador
const { Router } = require("express");
const router = Router();

//rutas -> routes para las api
//Se debe de tener en cuenta que aqui cada vez que se visite la rut '/' es cuando nos va mostrar el mensaje "Hello World"
router.get('/', (req, res) => {
    //res.send('Hello world');  el obejto send se encarga de enviar un string
    //ahora vamos vamos a utilizar un objeto JSON
    res.json({"Titulo": "Hello World"});
 });

//Ahora se puede hacer otro ejemplo que nos va a devovler un mensaje cuando ingresemos a la ruta /test
router.get('/test', (req, res) => {
    const data ={
        "name":"TheBryan",
        "Universidad":"USC Cali"
    }
    res.json({data});
 });

// con las lineas nuevas de codigo que se han creado en este archivo de rutas, podremos exportarlas a donde sean requeridas y generamos bajo acoplamiento
module.exports = router;