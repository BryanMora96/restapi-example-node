const { Router } = require('express');
const router = Router();

//hago el llamado a la biblioteca Underscore
const _ = require('underscore');

//se va a importar la clase donde se encuentra la base de  datos quemada, para hacer unso de los objetos JSON
const movies = require('../sample.json');
console.log(movies); //podemos observar el arrglo por la consola

router.get('/', (req, res) => {
    res.json(movies);
});

//Se realiza la busqueda de los registros a traves del :id
router.get('/:id', (req, res) => {
    const {id} = req.params;
        _.each(movies, (movie, i) => {
            //Se valida que el id que se esta enviando por URL, sea igual al id que se tiene en la base de datos 
            if(movie.id == id) {
                res.json(movie);
            }
        });
});

router.post('/', (req,res) => {
    console.log(req.body);
    //Se guardan  en constantes los registros del JSON
    const{ id, titulo,director,año,reating } = req.body;
    //Asi podemos realizar una validacion con los datos del JSON que nos esta llegando desde el POST
    if(titulo && director && año && reating){    
        const id = movies.length + 1;
        const nuevaPelicula = { id, ...req.body};
        movies.push(nuevaPelicula);
        res.json(movies);
        res.send('Se han almacenado los datos');
    }else{
        //Existen los codigos de estado, le dicen desde el servidor al cliente para que se entienda que ha sucedido
        //Estos estados los podemos observar a traves de de los comandos para desarrollador del navegador web
        //res.send('Los datos no se han gestionados en totalidad'); // Mensaje de error plano sin control por el servidor
        res.status(500).json({error: 'Ha ocurrido un error, diligencie todos los campos'});
    }
    res.send('recibido');
});

//Haremos un ejemplo para realizar una actualizacion de los registros
router.put('/:id', (req, res) => {
    const {id} = req.params;
    const {titulo,director,año,reating} = req.body;
    //Valida que se llenen todos los campos del JSON
    if(titulo && director && año && reating){
        _.each(movies, (movie, i) => {
            //Se valida que el id que se esta enviando por URL, sea igual al id que se tiene en la base de datos 
            if(movie.id == id) {
                //asigna los campos de la base de datos con los enviados por el JSON
                movie.titulo = titulo;
                movie.director = director;
                movie.año = año;
                movie.reating = reating;
            }
        });
        res.json(movies);
    }else {
        res.status(500).json({error: 'Ha ocurrido un error'});
    }
});

//Haremos un ejemplo para realizar una eliminacion de los registros
router.delete('/:id', (req, res) => {
    const {id} = req.params;
    _.each(movies, (movie, i) => {
        if(movie.id == id) {
            movies.splice(i, 1);
        }
    });
    res.send(movies);
});

module.exports = router;