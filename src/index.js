//creamos una constante, con el express que estamos requiridiendo.
const express = require('express');
//ejecutamos el framework de express y lo almacenamos en una constante y con esto ya podemos ejecutar la app.
const app = express();
//utilizaremos el otro modulo llamado morgan
const morgan = require('morgan');

//configuracion del servidore o settings
app.set('port', process.env.PORT || 3000); //La aplicación Node.js / Express.js solo funciona en el puerto 3000
app.set('json spaces', 2); //esto se hace para el espaciado en el JSON, de esta manera se puede interpretar mucho mejor el objeto JSON

//tenemos que empezar a ejecutarlo
//morgan es un middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

//aqui hacemos uso de las rutas que han sido creadas en el archivo index.js
app.use(require('./routes/index'));
app.use('/api/movies',require('./routes/movies'));

//starting the server
//le decimos a la app que escuche por el puerto 300
//y que cuando inicie, muestre el mesnaje que le estamos enviando
app.listen(app.get('port'), () => {
    console.log(`Server on port ${app.get('port')}`);
});
